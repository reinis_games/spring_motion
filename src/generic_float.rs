/// A trait to allow generic implementations of the system for both [f32] and [f64]
pub trait Scalar {
    const PI: Self;
    const ONE: Self;
    const TWO: Self;
    const FOUR: Self;

    fn square_root(self) -> Self;
}

impl Scalar for f32 {
    const PI: Self = std::f32::consts::PI;
    const ONE: Self = 1.;
    const TWO: Self = 2.;
    const FOUR: Self = 4.;

    fn square_root(self) -> Self {
        self.sqrt()
    }
}

impl Scalar for f64 {
    const PI: Self = std::f64::consts::PI;
    const ONE: Self = 1.;
    const TWO: Self = 2.;
    const FOUR: Self = 4.;

    fn square_root(self) -> Self {
        self.sqrt()
    }
}
